const { Mesh, MeshBasicMaterial, Color } = require('three');

const gui = require('../utils/gui');


const PROPS = {
  speed : 0.025,
  depth : 6,
  displacement : { x: 1.5, y : 3 },
  color : '#FF00BD',
};

// We only need one material for all letter (they look the same)
const material = new MeshBasicMaterial({
  color: new Color(PROPS.color),
});

// GUI
const letterGuiFolder = gui.addFolder('Letter');
letterGuiFolder.addColor(PROPS, 'color').onChange(() => {
  material.color = new Color(PROPS.color);
});
letterGuiFolder.add(PROPS, 'speed', 0.01, 0.1).name('speed');
letterGuiFolder.add(PROPS, 'depth', 1, 6).name('length');
letterGuiFolder.add(PROPS.displacement, 'x', 1, 5).name('disp X');
letterGuiFolder.add(PROPS.displacement, 'y', 1, 5).name('disp Y');

/**
 * Mesh based of a letter shape geometry
 */
class Letter extends Mesh {
  constructor(letterGeometry) {
    super(letterGeometry, material);

    this.update = this.update.bind(this);
  }

  update() {
    this.position.z -= PROPS.speed;
  }

  setProps({ x = 0, y = 0, scale = 1 } = {}) {
    this.position.x = x * PROPS.displacement.x;
    this.position.y = y * PROPS.displacement.y;
    this.scale.set(scale, scale, scale);
  }

  // Check if the letter become too far to be visible
  isDead() {
    return this.position.z < -PROPS.depth;
  }
}

module.exports = Letter;