
const {
  Group, MeshBasicMaterial, Mesh, SphereBufferGeometry, Color,
  TextureLoader, SpriteMaterial, Sprite,
} = require('three');
const random = require('canvas-sketch-util/random');

const NBR_OF_PARTICLES = 100;
const END = -4;
const BEGIN = 4;

class Particles extends Group {
  constructor() {
    super();

    const spriteMap = new TextureLoader().load( "../assets/flare.png" );
    const spriteMaterial = new SpriteMaterial( {
      map: spriteMap,
      color: 0xffffff,
      transparent: true,
      opacity: 0.3,
    });


    // Create the particles with random position, scale and speed
    for (let i = 0; i < NBR_OF_PARTICLES; i++) {
      const particle = new Sprite( spriteMaterial );
      particle.position.set(
        random.range(-3, 3),
        random.range(-1, 2),
        random.range(END, BEGIN)
      );
      particle.scale.multiplyScalar(random.range(0.01, 0.05));
      particle.userData.speed = random.range(0.002, 0.005);
      this.add(particle);
    }

    this.update = this.update.bind(this);
  }

  update() {
    // Update the position of each particles
    for (let i = 0; i < NBR_OF_PARTICLES; i++) {
      this.children[i].position.z -= this.children[i].userData.speed;

      // Reset the position of the particles
      if (this.children[i].position.z < END) {
        this.children[i].position.z = BEGIN;
      }
    }

  }
}

module.exports = Particles;