# Creative Coding Toronto - 2019/09/14

## Our Final Type - How to do web graphic design with code (quickly)?

Based to the work we did for the [36 Days Of Types](https://36days.reflektor.digital), let's go through the process of creation for another web experiment!

[![Final render](images/final.png)](https://creativecodingto-demo1.netlify.com/)

For this demo. my goal was to use the same spirit of the challenge 36 Days of Type has: Playing with letters!
And adding more interactions and motions to them.

## 1. Finding inspirations and establish a concept.

I believe finding inspirations go beyond the web. Like lots of creative people, I find inspirations from all kinda of mediums, including design, websites, developers, posters, movies, nature…

It's a long process but it will help you to keep your ideas grow to build up for your original concept.

Here are the inspirations for this project:

![@zach.lieberman inspiration](images/zachLiebermanInspiration.gif)
![@roy_cranston](images/royCranstonInspiration.png)
![B](images/jeremieBoulayInspiration.gif)

Inspired by Zach Lieberman [@zach.lieberman](https://twitter.com/zachlieberman?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor), Roy Cranston [@roy_cranston](https://www.behance.net/roycranston) and my original letter ([B](https://36days.reflektor.digital/letter/B)), I wanted to create a trail of letters who can move with regard user interactions.

And of course, I went back to pen and paper to pose my ideas somewhere :

![Sheme](images/concept.png)


## 2. Set up a quick and easy environment

Experimenting and create visual experiments should be fast, flexible and easy to improve. So you should have a really light coding environment.

Most of the letters are made with [canvas-sketch](https://github.com/mattdesl/canvas-sketch), an awesome framework by our friend [Matt Delauriers](https://www.mattdesl.com/).
This command line tool setup in one file, which gives you everything you need to create graphic render with code. It includes Hot Reloading, [three.js](https://threejs.org/) template and much more functionalities, like recording a video, saving an image...

So for this demo, we will start by creating a three.js template with this command :

`canvas-sketch --new --template=three --open index.js`


## 2. Define the global scene style

We tried to keep the same general ambient style for all of our letters. We use dark background and flashy colors, as per [Reflektor](https://reflektor.digital/) branding.

I also like to use a personal script called [CameraMouseMotion](https://gist.github.com/Jeremboo/376d0903d37a6e32fdeb63235869ceee) which adds some camera motion on mouse move and improves the dynamic of the 3D scene.

![Step 1 - global style](images/step1.png)

## 3. Develop your ideas

Then, according to our inspirations and the global style defined, we can finally start to create evolve our idea.

![Step 2](images/step2.gif)
![Step 3](images/step3.gif)
![Step 4](images/step4.gif)
![Step 5](images/step5.gif)

## 4. Use dat.GUI and controllers to finalize your render

The main issue you will encounter (after resolving all technical issues) will be to be perfect your work with the parameters you like. It could be taxing to go back and forth between your code and the render to change those numbers to achieve the effect you are looking for. Instead, having a handy tool, like [dat.GUI](http://workshop.chromeexperiments.com/examples/gui/#1--Basic-Usage), to help updating your variables right in the browser could make your development experience much enjoyable.

![Dat.GUI](images/gui.gif)


## 5. Possible improvements (TODO):

 - Create a shader material with noise and grain (like the 5 exp)
 - Do a multicolor trail as konami code
 - Create multiple trails