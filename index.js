const canvasSketch = require('canvas-sketch');
const gui = require('./utils/gui');

const Letter = require('./objects/Letter');
const Particles = require('./objects/Particles');

const CameraMouseMotion = require('./utils/CameraMouseMotion');
const fontFile = require('./utils/font');

// Ensure ThreeJS is in global scope for the 'examples/'
global.THREE = require('three');

const settings = {
  // Make the loop animated
  animate: true,
  // Get a WebGL canvas rather than 2D
  context: 'webgl',
  // Turn on MSAA
  attributes: {
    antialias: true,
    transparent: true
  }
};

const sketch = ({ context }) => {
  // Constants props
  const MAIN_COLOR   = '#040507';
  const SECOND_COLOR = '#3F195E';

  const PROPS = {};

  const WORD = 'REFLEKTOR';

  // Create a renderer
  const renderer = new THREE.WebGLRenderer({
    context
  });

  // CSS gradient color. The easiest way to have a gradient in the background
  document.body.style.backgroundImage = `linear-gradient(120deg, ${MAIN_COLOR} 40%, ${SECOND_COLOR} 125%)`;

  // Setup a camera
  const camera = new THREE.PerspectiveCamera(45, 1, 0.01, 100);
  camera.position.set(-2.7, 2, 3.5);

  // Setup camera controller
  const cameraMotion = new CameraMouseMotion(camera, {
    motion   : new THREE.Vector2(1, 2),
    Vel : new THREE.Vector2(0.1, 0.1),
    target   : new THREE.Vector3(-0.05, 0.05, -0.4),
  });

  // Setup your scene
  const scene = new THREE.Scene();
  scene.fog   = new THREE.Fog(new THREE.Color(MAIN_COLOR), 2, 8);

  // Specify an ambient/unlit colour
  scene.add(new THREE.AmbientLight('#ffffff'));

  // * STEP 1 : Create a Letter Mesh --------------------------------------------------------------------------------------------
  // * --------------------------------------------------------------------------------------------------------------------------

  // Init the font loader by using a JSON font file.
  // See : https://threejs.org/docs/index.html#api/en/loaders/FontLoader
  const fontLoader = new THREE.FontLoader();
  const font = fontLoader.parse(fontFile);

  let currentLetterGeomIdx = 0;
  const letterGeoms = {};

  function createLetterGeometry(letter) {
    const geom = new THREE.ShapeBufferGeometry(
      font.generateShapes(letter, 1)
    );
    // Create our letter geometry and centerise it
    geom.computeBoundingBox();
    geom.translate(
      -geom.boundingBox.max.x * 0.5,
      -geom.boundingBox.max.y * 0.5,
      0
    );
    return geom;
  }

  function getCurrentGeom() {
    return letterGeoms[WORD[currentLetterGeomIdx]];
  }

  // For each letter, create the geometry
  for (let i = 0; i < WORD.length; i++) {
    const letter = WORD[i];
    letterGeoms[letter] = createLetterGeometry(letter);
  }

  // * STEP 2 : Create and animate a trail of letters  --------------------------------------------------------------------------------
  //*  --------------------------------------------------------------------------------------------------------------------------------

  let elapsedTime = 0;
  PROPS.interval = 0.08;

  // Instanciate a letter at the center
  const letterArray = [];
  function addLetter(geometry, props) {
    const letter = new Letter(geometry);
    letter.setProps(props);
    letterArray.push(letter);
    scene.add(letter);
  }

  // Instantiate a letter who doesn't be updated to stay in front of the others
  let frontLetter = null;
  function initFrontLetter(geometry, props) {
    // Remove the previous front letter
    if (frontLetter) scene.remove(frontLetter);
    // Create a new instance
    frontLetter = new Letter(geometry);
    frontLetter.setProps(props);
    scene.add(frontLetter);
  }

  initFrontLetter(getCurrentGeom());

  // * STEP 3 : Add mouse interaction ---------------------------------------------------------------------------------------------------
  // * ----------------------------------------------------------------------------------------------------------------------------------

  const letterProps = { x: 0, y: 0, scale: 1 };

  let scaleVel = 0.2;
  let positionVel = 0.5;

  let targetedScale = 1;
  PROPS.targetedScaleDown = 0.2;
  const targetedMousePos = { x : 0, y : 0 };

  document.body.addEventListener('mousemove', (event) => {
    const normalizedMouseX = (event.clientX / window.innerWidth) - 0.5;
    const normalizedMouseY = 1 - (event.clientY / window.innerHeight) - 0.5;

    targetedMousePos.x = normalizedMouseX;
    targetedMousePos.y = normalizedMouseY;
  });

  document.body.addEventListener('mousedown', () => {
    scaleVel = 0.05;
    positionVel = 0.1;
    targetedScale = PROPS.targetedScaleDown;
  });

  document.body.addEventListener('mouseup', () => {
    scaleVel = 0.02;
    positionVel = 0.05;
    targetedScale = 1;
  });

  // * STEP 5 : Smooth the interactions -------------------------------------------------------------------------------------------------
  // * ----------------------------------------------------------------------------------------------------------------------------------

  // Based on :
  // position += (targetedPosition - position) * Vel
  function updateLetterPropsSmoothly() {
    letterProps.x += (targetedMousePos.x - letterProps.x) * positionVel;
    letterProps.y += (targetedMousePos.y - letterProps.y) * positionVel;
    letterProps.scale += (targetedScale - letterProps.scale) * scaleVel;
  }

  // * STEP 6 : Add a GUI ---------------------------------------------------------------------------------------------------------------
  // * ----------------------------------------------------------------------------------------------------------------------------------
  gui.add(PROPS, 'interval', 0, 0.15);
  gui.add(PROPS, 'targetedScaleDown', 0, 2).name('scaleDown');

  // * STEP 7 : Add particles -----------------------------------------------------------------------------------------------------------
  // * ----------------------------------------------------------------------------------------------------------------------------------
  const particles = new Particles();
  scene.add(particles);

  // * STEP 8 : Add key interactions ----------------------------------------------------------------------------------------------------
  // * ----------------------------------------------------------------------------------------------------------------------------------
  document.body.addEventListener('keydown', () => {
    // Create the finale letter
    addLetter(getCurrentGeom(), letterProps);

    // Hack the animations parameters to create a visual effect
    elapsedTime = -0.1;
    letterProps.scale = 0.1;

    // Increment the geometry index
    currentLetterGeomIdx = (currentLetterGeomIdx + 1) % (WORD.length - 1);

    // Reset the front letter
    initFrontLetter(getCurrentGeom(), letterProps);
  });

  // draw each frame
  return {
    // Handle resize events here
    resize ({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio);
      renderer.setSize(viewportWidth, viewportHeight);
      camera.aspect = viewportWidth / viewportHeight;
      camera.updateProjectionMatrix();
    },
    // Update & render your scene here
    render ({ deltaTime }) {
      cameraMotion.update();

      updateLetterPropsSmoothly();

      // Instantiate a new letter each interval
      elapsedTime += deltaTime;
      if (elapsedTime > PROPS.interval) {
        elapsedTime = 0;
        addLetter(getCurrentGeom(), letterProps);
      }

      // Update the letters
      for (let i = 0; i < letterArray.length; i++) {
        letterArray[i].update();
      }

      // Remove the dead letters by checking from the oldest ones until one of them stays alive
      while (letterArray[0] && letterArray[0].isDead()) {
        const letter = letterArray.shift();
        scene.remove(letter);
      }

      // Update the props of the letter in front
      frontLetter.setProps(letterProps);

      particles.update();

      renderer.render(scene, camera);
    },
    // Dispose of events & renderer for cleaner hot-reloading
    unload () {
      cameraMotion.dispose();
      renderer.dispose();
    }
  };
};

canvasSketch(sketch, settings);
