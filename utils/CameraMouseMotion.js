const { Vector2, Vector3 } = require('three');

/**
 * @author jeremboo https://jeremieboulay.fr
 * @version 1.1.0
 * @source https://gist.github.com/Jeremboo/376d0903d37a6e32fdeb63235869ceee
 *
 * CameraMouseMotion
 * -------------
 * Add some smooth camera motion depending on the mouse move.
 *
 *
 * Basic use:
 * -------------
 *
 * // Create a camera && set the position waned
 * const camera = new THREE.PerspectiveCamera(45, 1, 0.01, 100);
 * camera.position.set(-2.7, 2, 3.5);
 *
 * // Setup the cameraMotion
 * const cameraMotion = new CameraMouseMotion(camera, {
 *   motion   : new THREE.Vector2(1, 2),
 *   velocity : new THREE.Vector2(0.1, 0.1),
 *   target   : new THREE.Vector3(-0.05, 0.05, -0.4),
 * });
 *
 *
 * // Don't forget to update the cameraMotion every frame
 * update() {
 *  cameraMotion.update();
 * }
 *
 */
class CameraMouseMotion {
  constructor(camera, {
    target   = new Vector3(),
    motion   = new Vector2(1, 1),
    velocity = new Vector2(0.1, 0.1),
  } = {}) {
    this.camera   = camera;
    this.motion   = motion;
    this.velocity = velocity;
    this.target   = target;
    this.initialPos = camera.position.clone();
    this.targetedPos = camera.position.clone();

    this.handleMouseMove = this.handleMouseMove.bind(this);
    this.update = this.update.bind(this);
    this.dispose = this.dispose.bind(this);

    document.body.addEventListener('mousemove', this.handleMouseMove);
  }
  handleMouseMove(event) {
    this.targetedPos.x = this.initialPos.x - (((event.clientX / window.innerWidth) - 0.5) * this.motion.x);
    this.targetedPos.y = this.initialPos.y + (((event.clientY / window.innerHeight) - 0.5) * this.motion.y);
  }
  update() {
    this.camera.position.x += (this.targetedPos.x - this.camera.position.x) * this.velocity.x;
    this.camera.position.y += (this.targetedPos.y - this.camera.position.y) * this.velocity.y;
    this.camera.lookAt(this.target);
  }
  dispose() {
    document.body.removeEventListener('mousemove', this.handleMouseMove);
  }
}

module.exports = CameraMouseMotion;